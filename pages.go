// Package pages is a lightweight tool to handle pagination for requests
package pages

// Page is a page of data
type Page struct {
	// We need a way to say whether or not we want to serialize Previous...
	Previous *int        `json:"previous,omitempty"`
	Next     int         `json:"next,omitempty"`
	Count    int64       `json:"count,omitempty"`
	Results  interface{} `json:"results"`
}

// Lister returns a list of objects given an offset and a limit
type Lister func(int, int) (interface{}, error)

// Counter counts objects
type Counter func() (int64, error)

// Query returns a query parameter
// This is called with the "page" (defaults to 0) and "count" (defaults to 20) values
type Query func(string) string

// PageQuery is the name of the query param for the page index
const PageQuery = "page"

// DefaultPage is the default page index
const DefaultPage = 0

// CountQuery is the name of the query param for the number of items per page
const CountQuery = "count"

// DefaultCount is the default number of items per page
const DefaultCount = 20

// OffsetLimitPage returns the offset, limit and page index given a query function
func OffsetLimitPage(query Query) (int, int, int) {
	index := getQueryInt(query, PageQuery, DefaultPage)
	count := getQueryInt(query, CountQuery, DefaultCount)

	return index * count, count, index
}

// Make handles pagination
func Make(q Query, lister Lister, counter Counter) (*Page, error) {
	offset, limit, index := OffsetLimitPage(q)

	done := make(chan error, 1)

	page := Page{}

	go countRecords(counter, done, &page.Count)

	objects, err := lister(offset, limit)
	if err != nil {
		return nil, err
	}
	if err = <-done; err != nil {
		return nil, err
	}

	page.Results = objects
	if index > 0 {
		previous := index - 1
		page.Previous = &previous
	}
	if page.Count > int64(offset+limit) {
		page.Next = index + 1
	}

	return &page, nil
}

func countRecords(counter Counter, done chan error, count *int64) {
	var err error
	*count, err = counter()
	done <- err
}
